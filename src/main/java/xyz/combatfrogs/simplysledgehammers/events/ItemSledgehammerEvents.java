package xyz.combatfrogs.simplysledgehammers.events;

import net.minecraft.block.Block;
import net.minecraft.enchantment.EnchantmentHelper;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Blocks;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraftforge.event.world.BlockEvent;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import xyz.combatfrogs.simplysledgehammers.ModItems;
import xyz.combatfrogs.simplysledgehammers.items.tools.ItemSledgehammer;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;

/**
 * Created by Christopher on 1/12/2016.
 */
public class ItemSledgehammerEvents {
    private static final Map<Item, Item> sledgingMap = new HashMap<Item, Item>() {{
        put(Item.getItemFromBlock(Blocks.iron_ore), ModItems.ironOreChunk);
        put(Item.getItemFromBlock(Blocks.gold_ore), ModItems.goldOreChunk);
    }};

    private static final Random randGen = new Random();

    public ItemSledgehammerEvents() {

    }

    @SubscribeEvent
    public void onHarvestBlocks(BlockEvent.HarvestDropsEvent event)
    {
        EntityPlayer player = event.harvester;
        ItemStack heldItem = player.inventory.getCurrentItem();
        Block block = event.state.getBlock();
        Item blockItem = Item.getItemFromBlock(block);

        int fortuneLevel = EnchantmentHelper.getFortuneModifier(player);

        if (heldItem.getItem() instanceof ItemSledgehammer
                && sledgingMap.containsKey(blockItem)) {
            event.drops.clear();

            int minQuantity, maxQuantity;
            switch(fortuneLevel) {
                case 1:
                    minQuantity = 8;
                    maxQuantity = 9;
                    break;
                case 2:
                    minQuantity = 10;
                    maxQuantity = 12;
                    break;
                case 3:
                    minQuantity = 12;
                    maxQuantity = 14;
                    break;
                default:
                    minQuantity = 7;
                    maxQuantity = 9;
                    break;
            }

            event.drops.add(new ItemStack(sledgingMap.get(blockItem),
                    randGen.nextInt(maxQuantity - minQuantity) + minQuantity));
            event.drops.add(new ItemStack(ModItems.cobblestones, 5));
            event.dropChance = 1.0f;
        }
    }
}
