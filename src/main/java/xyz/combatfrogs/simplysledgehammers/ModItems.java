package xyz.combatfrogs.simplysledgehammers;

import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;
import net.minecraftforge.fml.common.registry.GameRegistry;
import xyz.combatfrogs.simplysledgehammers.items.tools.ItemSledgehammer;

public class ModItems {

    public static ItemSledgehammer woodSledgehammer;
    public static ItemSledgehammer stoneSledgehammer;
    public static ItemSledgehammer ironSledgehammer;
    public static ItemSledgehammer goldSledgehammer;
    public static ItemSledgehammer diamondSledgehammer;

    // CRAFTING MATERIALS
    public static Item ironOreChunk;
    public static Item goldOreChunk;
    public static Item cobblestones;
    public static Item ironUnsmeltedIngot;
    public static Item goldUnsmeltedIngot;

    public static void init() {
        // SLEDGEHAMMERS
        woodSledgehammer = new ItemSledgehammer(Item.ToolMaterial.WOOD);
        GameRegistry.registerItem(woodSledgehammer);
        stoneSledgehammer = new ItemSledgehammer(Item.ToolMaterial.STONE);
        GameRegistry.registerItem(stoneSledgehammer);
        ironSledgehammer = new ItemSledgehammer(Item.ToolMaterial.IRON);
        GameRegistry.registerItem(ironSledgehammer);
        goldSledgehammer = new ItemSledgehammer(Item.ToolMaterial.GOLD);
        GameRegistry.registerItem(goldSledgehammer);
        diamondSledgehammer = new ItemSledgehammer(Item.ToolMaterial.EMERALD);
        GameRegistry.registerItem(diamondSledgehammer);

        // ORE CHUNKS
        ironOreChunk = new Item()
                .setUnlocalizedName(Reference.MODID + "_iron_ore_chunk")
                .setRegistryName("iron_ore_chunk")
                .setCreativeTab(CreativeTabs.tabMaterials);
        GameRegistry.registerItem(ironOreChunk);

        goldOreChunk = new Item()
                .setUnlocalizedName(Reference.MODID + "_gold_ore_chunk")
                .setRegistryName("gold_ore_chunk")
                .setCreativeTab(CreativeTabs.tabMaterials);
        GameRegistry.registerItem(goldOreChunk);

        // UNSMELTED INGOTS
        ironUnsmeltedIngot = new Item()
                .setUnlocalizedName(Reference.MODID + "_iron_unsmelted_ingot")
                .setRegistryName("iron_unsmelted_ingot")
                .setCreativeTab(CreativeTabs.tabMaterials);
        GameRegistry.registerItem(ironUnsmeltedIngot);
        goldUnsmeltedIngot = new Item()
                .setUnlocalizedName(Reference.MODID + "_gold_unsmelted_ingot")
                .setRegistryName("gold_unsmelted_ingot")
                .setCreativeTab(CreativeTabs.tabMaterials);
        GameRegistry.registerItem(goldUnsmeltedIngot);

        cobblestones = new Item()
                .setUnlocalizedName(Reference.MODID + "_cobblestones")
                .setRegistryName("cobblestones")
                .setCreativeTab(CreativeTabs.tabMaterials);
        GameRegistry.registerItem(cobblestones);
    }
}