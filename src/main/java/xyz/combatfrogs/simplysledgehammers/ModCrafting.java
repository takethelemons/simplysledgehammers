package xyz.combatfrogs.simplysledgehammers;

import net.minecraft.init.Blocks;
import net.minecraft.init.Items;
import net.minecraft.item.ItemStack;
import net.minecraftforge.fml.common.registry.GameRegistry;

/**
 * Created by Christopher on 1/10/2016.
 */
public class ModCrafting {
    public static void init() {
        ItemStack stickStack = new ItemStack(Items.stick, 1);

        // SLEDGEHAMMERS
        GameRegistry.addRecipe(new ItemStack(ModItems.woodSledgehammer, 1),
                "xxx",
                "xyx",
                " y ",
                'x', new ItemStack(Blocks.planks, 1),
                'y', stickStack);
        GameRegistry.addRecipe(new ItemStack(ModItems.stoneSledgehammer, 1),
                "xxx",
                "xyx",
                " y ",
                'x', new ItemStack(Blocks.cobblestone, 1),
                'y', stickStack);
        GameRegistry.addRecipe(new ItemStack(ModItems.ironSledgehammer, 1),
                "xxx",
                "xyx",
                " y ",
                'x', new ItemStack(Items.iron_ingot, 1),
                'y', stickStack);
        GameRegistry.addRecipe(new ItemStack(ModItems.goldSledgehammer, 1),
                "xxx",
                "xyx",
                " y ",
                'x', new ItemStack(Items.gold_ingot, 1),
                'y', stickStack);
        GameRegistry.addRecipe(new ItemStack(ModItems.diamondSledgehammer, 1),
                "xxx",
                "xyx",
                " y ",
                'x', new ItemStack(Items.diamond, 1),
                'y', stickStack);

        // COBBLESTONES -> COBBLESTONE (BLOCK)
        ItemStack cobblestonesStack = new ItemStack(ModItems.cobblestones, 1);
        GameRegistry.addRecipe(new ItemStack(Blocks.cobblestone, 1),
                "xxx",
                "xxx",
                "xxx",
                'x', cobblestonesStack);


        // ORE CHUNKS -> UNSMELTED INGOTS
        ItemStack ironOreChunkStack = new ItemStack(ModItems.ironOreChunk, 1);
        GameRegistry.addShapelessRecipe(new ItemStack(ModItems.ironUnsmeltedIngot),
                ironOreChunkStack,
                ironOreChunkStack,
                ironOreChunkStack,
                ironOreChunkStack);

        ItemStack goldOreChunkStack = new ItemStack(ModItems.ironOreChunk, 1);
        GameRegistry.addShapelessRecipe(new ItemStack(ModItems.goldUnsmeltedIngot),
                goldOreChunkStack,
                goldOreChunkStack,
                goldOreChunkStack,
                goldOreChunkStack);

        // UNSMELTED INGOTS -> INGOTS
        GameRegistry.addSmelting(ModItems.ironUnsmeltedIngot, new ItemStack(Items.iron_ingot, 1), 0.0f);
        GameRegistry.addSmelting(ModItems.goldUnsmeltedIngot, new ItemStack(Items.gold_ingot, 1), 0.0f);
    }
}
