package xyz.combatfrogs.simplysledgehammers;

/**
 * Created by Christopher on 1/13/2016.
 */
public final class Reference {
    public static final String MODID = "simplysledgehammers";
    public static final String MODNAME = "Simply Sledgehammers";

    private Reference() { }
}
