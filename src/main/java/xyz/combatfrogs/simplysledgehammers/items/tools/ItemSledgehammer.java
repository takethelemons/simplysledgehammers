package xyz.combatfrogs.simplysledgehammers.items.tools;

import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.ItemPickaxe;
import xyz.combatfrogs.simplysledgehammers.Reference;

/**
 * Created by Christopher on 1/12/2016.
 */
public final class ItemSledgehammer extends ItemPickaxe {
    public ItemSledgehammer(ToolMaterial material) {
        super(material);
        setUnlocalizedName(Reference.MODID + '_' + toolMaterial.name().toLowerCase() + "_hammer");
        setRegistryName(toolMaterial.name().toLowerCase() + "_hammer");
        setCreativeTab(CreativeTabs.tabTools);
    }
}
