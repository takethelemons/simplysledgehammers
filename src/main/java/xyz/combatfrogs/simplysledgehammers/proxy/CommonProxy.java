package xyz.combatfrogs.simplysledgehammers.proxy;

import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPostInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;
import xyz.combatfrogs.simplysledgehammers.ModCrafting;
import xyz.combatfrogs.simplysledgehammers.ModItems;

/**
 * Created by Christopher on 1/13/2016.
 */
public class CommonProxy {
    public void preInit(FMLPreInitializationEvent e) {
        ModItems.init();
        ModCrafting.init();
    }

    public void init(FMLInitializationEvent e) {

    }

    public void postInit(FMLPostInitializationEvent e) {

    }
}
