package xyz.combatfrogs.simplysledgehammers.proxy;

import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.entity.RenderItem;
import net.minecraft.client.resources.model.ModelResourceLocation;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;
import xyz.combatfrogs.simplysledgehammers.ModItems;
import xyz.combatfrogs.simplysledgehammers.Reference;

/**
 * Created by Christopher on 1/13/2016.
 */
public class ClientProxy extends CommonProxy {
    @Override
    public void preInit(FMLPreInitializationEvent e) {
        super.preInit(e);
    }

    @Override
    public void init(FMLInitializationEvent e) {
        super.init(e);

        RenderItem renderItem = Minecraft.getMinecraft().getRenderItem();

        renderItem.getItemModelMesher().register(ModItems.woodSledgehammer, 0,
                new ModelResourceLocation(Reference.MODID + ":wood_hammer", "inventory"));
        renderItem.getItemModelMesher().register(ModItems.stoneSledgehammer, 0,
                new ModelResourceLocation(Reference.MODID + ":stone_hammer", "inventory"));
        renderItem.getItemModelMesher().register(ModItems.ironSledgehammer, 0,
                new ModelResourceLocation(Reference.MODID + ":iron_hammer", "inventory"));
        renderItem.getItemModelMesher().register(ModItems.goldSledgehammer, 0,
                new ModelResourceLocation(Reference.MODID + ":gold_hammer", "inventory"));
        renderItem.getItemModelMesher().register(ModItems.diamondSledgehammer, 0,
                new ModelResourceLocation(Reference.MODID + ":emerald_hammer", "inventory"));

        renderItem.getItemModelMesher().register(ModItems.ironOreChunk, 0,
                new ModelResourceLocation(Reference.MODID + ":iron_ore_chunk", "inventory"));
        renderItem.getItemModelMesher().register(ModItems.goldOreChunk, 0,
                new ModelResourceLocation(Reference.MODID + ":gold_ore_chunk", "inventory"));
        renderItem.getItemModelMesher().register(ModItems.cobblestones, 0,
                new ModelResourceLocation(Reference.MODID + ":cobblestones", "inventory"));
        renderItem.getItemModelMesher().register(ModItems.ironUnsmeltedIngot, 0,
                new ModelResourceLocation(Reference.MODID + ":iron_unsmelted_ingot", "inventory"));
        renderItem.getItemModelMesher().register(ModItems.goldUnsmeltedIngot, 0,
                new ModelResourceLocation(Reference.MODID + ":gold_unsmelted_ingot", "inventory"));
    }
}
